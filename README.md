# R_projet_csd



## Objectif

L'objectif de ce projet est l'automatisation de rapports statistiques comprenant des statistqiues descriptives pour chaque variable d'un jeu de données.

Ces rapports sont générés à partir de datasets devant être nommés sous la forme 'data_ * .csv' et accompagnés de leur cahier de variables 'variables_* .csv'

## Fichiers .R et .Rmd

Le fichier parcours_dossier.R est le script permettant d'automatiser la génération des rapports.
Il fait appel au fichier rapport.Rmd qui génère les rapports.


## Dossiers

Dans les dossiers se trouvent les jeux de données (cahiers de variables si existants) tels qu'ils ont été récupérés. 
Lorsqu'un rapport a pu être généré il se situe dans le dossier correspondant.


## Auto-évaluation

Question | Objectif atteint (Oui/Non) | Qualité du code 
--- | --- | --- 
Fonctions descriptives | Oui | bon
Parcours de dossier| Oui | peu commenté
Parcours de dossier 2 | Oui | bon
Construction Rmarkdown | Oui | bon
Exécution Rmarkdown | Oui | bon 

## Auteur
Réalisé par Chloé Saint-Dizier dans le cadre du Master 1 Data Science en Sante, supervisé par M. Antoine Lamer.




